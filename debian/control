Source: nsf
Section: libs
Priority: optional
Maintainer: Tcl/Tk Debian Packagers <pkg-tcltk-devel@lists.alioth.debian.org>
Uploaders: Stefan Sobernig <stefan.sobernig@wu.ac.at>
Build-Depends: debhelper-compat (= 13), tcl-dev, tcllib
Standards-Version: 4.7.0
Homepage: https://next-scripting.org/
Vcs-Git: https://salsa.debian.org/tcltk-team/nsf.git
Vcs-Browser: https://salsa.debian.org/tcltk-team/nsf

Package: nsf
Architecture: any
Depends: tcl (>= 8.5), ${shlibs:Depends}, ${misc:Depends}
Suggests: nsf-shells
Description: Next Scripting Framework (NSF): Object orientation for Tcl - shared library
 The Next Scripting Framework (for short: NSF) is an infrastructure
 for creating object-oriented scripting languages based on Tcl. This package
 provides two ready-made object-orientated extensions for Tcl based on
 NSF: Next Scripting Language (NX, pronounced "next") and Extended
 Object Tcl version 2 (XOTcl2, pronounced "exotickle").

Package: nsf-dev
Section: libdevel
Architecture: any
Depends: nsf (= ${binary:Version}), ${misc:Depends}
Description: Next Scripting Framework (NSF): Object orientation for Tcl - development files
 Header files and static libraries for libnsf. This package must be in
 place if you plan to develop using the NSF/C interface and/or if you
 want to create custom builds of NSF/C extensions.

Package: nsf-shells
Section: interpreters
Architecture: all
Depends: nsf, ${misc:Depends}
Recommends: tk
Conflicts: xotcl-shells
Description: Next Scripting Framework (NSF): Object orientation for Tcl - shells
 This package provides you with scripted shell wrappers for the two
 NSF-builtin Tcl extensions: NX (nxsh, nxwish) and XOTcl2 (xotclsh,
 xowish).

