2019-05-07  Gustaf Neumann  <neumann@wu-wien.ac.at>


	Update release steps


	Silence clang 8 static checker


	improve documentation

2019-05-06  Stefan Sobernig  <stefan.sobernig@wu.ac.at>


	Updated changelog, fixed stats and indentation in announcement [skip ci]


	  * nsf-gridfs.test: Add one more piece of harness to test suite [skip ci]


	  * library/mongodb/*: Ran NSF MongoDB tests, against MongoDB 4.0.9 and
	Mongo-C driver 1.14.0 (both, latest stable); updated README.md
	accordingly, and fixed gridfs tests that hadn't been corrected for
	renaming README to README.md

2019-05-05  Gustaf Neumann  <neumann@wu-wien.ac.at>


	make spaces after comma regular


	improve grammar and spelling


	avoid one-liner loop

2019-05-03  Stefan Sobernig  <stefan.sobernig@wu.ac.at>


	  * nx.tcl, properties.test: Add exists accessor to properties and
	  variables; incl. tests and documentation


	Updated changelog, fixed stats in announcement [skip ci]


	Regenerated documentation


	  * Object.man: Remove line break in script evaluation


	Bump version number from 2.3a0 to 2.3

2019-05-02  Gustaf Neumann  <neumann@wu-wien.ac.at>


	reduce dead store operations and null after dereference

2019-05-01  Gustaf Neumann  <neumann@wu-wien.ac.at>


	improve spelling

2019-04-30  Stefan Sobernig  <stefan.sobernig@wu.ac.at>


	  * tests/*.test: Add 'package prefer latest' to remaining test files.


	Fix typos in ChangeLog [skip ci]


	Fix a remainder in changelog [skip ci]


	Done with changelog and announcement [skip ci]

2019-04-26  Stefan Sobernig  <stefan.sobernig@wu.ac.at>


	  * forward.test: Constrain the max recursion depth around recursive
	forwards, so that we do not run into early crashes on systems with
	limited stack sizes (when the stack is saturated before a recursion
	limit is hit). This fixes test runs on MinGW/ gcc builds at AppVeyor.

2019-04-23  Stefan Sobernig  <stefan.sobernig@wu.ac.at>


	  * forward.tests: Provide some background comment on platform-dependent
	error messages as test conditions (infinite loop) and make sure tests
	under 8.6 are executed as intended.


	Continued work in changelog and announcement (still WIP) [skip ci]

2019-04-21  Gustaf Neumann  <neumann@wu-wien.ac.at>


	added "pure" declarations


	removed null test after dereferences, avoid potential null dereferences
	in error cases


	removed null test after dereferences

2019-04-20  Gustaf Neumann  <neumann@wu-wien.ac.at>


	improve code safety and remove dead statement


	get rid of warning of static checker, reduce number of returns before
	end of function

2019-04-19  Gustaf Neumann  <neumann@wu-wien.ac.at>


	minor code cleanup


	add asserts

2019-04-19  Stefan Sobernig  <stefan.sobernig@wu.ac.at>


	Continued work in changelog and announcement (still WIP) [skip ci]

2019-04-18  Gustaf Neumann  <neumann@wu-wien.ac.at>


	fix memory leak on "hyper-volatile" objects. it seems, we have to set
	the _is_autonamed flag in advance of obj initialization

2019-04-17  Gustaf Neumann  <neumann@wu-wien.ac.at>


	addressing coverty defect 337829 (dereference before null check)

2019-04-15  Gustaf Neumann  <neumann@wu-wien.ac.at>


	make spelling more consistent


	fix typo


	Provide a more conservative change for setting autonamed flag


	provide more context info when the version mismatch test fails


	remove "-Wabi" for standard intense checking
	- more troubles then benefits


	Improve consistency of naming nsf objects

2019-04-15  Stefan Sobernig  <stefan.sobernig@wu.ac.at>


	  * nsf.c (NsfCCreateMethod): Fix access of potentially freed object to
	set the autonamed property (indicated by valdgrind). When destroyed
	and cleaned during DoObjInitialization, "newObject" will remain as a
	dangling pointer. Besides, setting the property before
	DoObjInitialization will make the object property available from within
	the initcmd or init method.


	  * nsf.c (NsfProcStubDeleteProc): Plug leakage of command structure
	  (found by valgrind)

2019-04-14  Stefan Sobernig  <stefan.sobernig@wu.ac.at>


	Add changelog and announcement files (WIP) [skip ci]

2019-04-12  Gustaf Neumann  <neumann@wu-wien.ac.at>


	muniro formatting changes

2019-04-12  Stefan Sobernig  <stefan.sobernig@wu.ac.at>


	Provide 2.2 builds on Appveyor


	Use maintenance branch for 2.2, to fulfil presence requirements of
	build descriptor


	Provide for 2.2.0 builds, and fix allowed failures


	Deploy mode for Appveyor builds

2019-04-11  Stefan Sobernig  <stefan.sobernig@wu.ac.at>


	Add missing install instruction [skip travis]


	Fix yaml [skip travis]


	Fix yaml [skip travis]


	Fix yaml [skip travis]


	Add deploy key to AppVeyor [skip travis]

2019-04-10  Stefan Sobernig  <stefan.sobernig@wu.ac.at>


	Remove context noise (directory tree) from the tarballs


	  * configure, configure.ac: Make sure 'git describe' uses --always, to
	  not fail on truncated checkouts without tags in reach (as on Travis)


	Provide some harness around 'make test', to avoid swallowing failing
	test suites in light of the write-error condition


	  * nsf.c (NsfDebugShowObj): For consistency, stick with %x using PRIxPTR


	*nsf.c (NsfDebugShowObj): Simplify from using %x to %p, to avoid
	PTR2UINT (which, starting with 8.7, will return u long, not u int)

2019-04-09  Stefan Sobernig  <stefan.sobernig@wu.ac.at>


	Completed rewrite, ufff


	Another try


	Fix gcc version for macos


	Rewrite build matrix (testing)


	Refine build matrix


	Refine build matrix


	Make tarball names more explanatory


	linux image requires more generic fix, Python module variant not
	available


	Change script block


	Test NONBLOCK fix for write error


	Change http:// occurrences to https://, if applicable (URLs tested)


	Fix permissions


	Avoid ssh-add


	Avoid host check


	Fix ssh/scp calls


	Re-enable install target


	Add a debugging statement around make test

2019-04-08  Stefan Sobernig  <stefan.sobernig@wu.ac.at>


	Stepping back, continued


	Stepping back, sigh


	Attempt at write-error fix


	Leave it be, for the moment


	Don't stop on stderr print-outs


	Make calls less verbose, are we hitting a limit?


	Avoid colliding paths


	Activate some introspection


	Fold separate bash calls into one


	Fix configure path


	Now with com


	Rebuild key, once again


	Adding new key


	Add build script with install target


	Fix glitch in openssl call


	Test for auto-deployment of builds


	Prepare for automated deploymnent of build artifacts [skip travis]


	  * apps/build.tcl: Revert URL change for build script, archive is
	  retrieved broken otherwise?


	Rewrite URLs containing tcl.tk as authority to tcl-lang.org

2019-03-31  Gustaf Neumann  <neumann@wu-wien.ac.at>


	improve spelling

2019-03-29  Gustaf Neumann  <neumann@wu-wien.ac.at>


	move new assertion to the right place


	improve wording


	ease human tracing of uninitialized/NULL values

2019-03-29  Stefan Sobernig  <stefan.sobernig@wu.ac.at>


	Fix test

2019-03-28  Stefan Sobernig  <stefan.sobernig@wu.ac.at>


	Complete caching by testing for kits


	fix descriptor


	Add caching support to travis descriptor

2019-03-27  Gustaf Neumann  <neumann@wu-wien.ac.at>


	add generated configure file for change
	e3968e8c972a8ac13bfaba27fcf2ae9e36689211


	address dtrace triggers from Makefile, although not configured


	try to simplify Makefile dependencies for DTRACE

2019-03-22  Stefan Sobernig  <stefan.sobernig@wu.ac.at>


	  * nsf.c (NsfOUplevelMethod, NsfOUpvarMethod): Silence compiler warnings
	  on nonnull/NULL compares.


	  * nsf.c (NsfOUplevelMethod, NsfOUpvarMethod,
	  NsfCallstackFindCallingContext): Reform of uplevel and
	  upvar methods, based on the recent feedback by Zoran.
	  First, uplevel and upvar methods, as well as [current
	  callinglevel] now behave consistently (re frame skipping
	  for mixin and filter transparency). If there is no
	  innermost enclosing proc frame, the innermost non-proc
	  (e.g., namespace) frame is selected (rather than a "#0"
	  as default). Second, argument handling for both uplevel
	  (i.e., level sniffing in multi-arg case) and upvar (e.g.,
	  made silent TCL_ERROR for invalid argument arities
	  explicit) have been fixed.
	  * Object.man, methods.test: Added documentation for both methods
	  (Object.man) and tests.

2019-03-22  Gustaf Neumann  <neumann@wu-wien.ac.at>


	reduce dead assignments improve indentation of variables


	avoid variable name "index" in generated code since "index" shadows an
	outer function

2019-03-21  Gustaf Neumann  <neumann@wu-wien.ac.at>


	mongodb: add "bson asJSON" convenience method

2019-03-20  Stefan Sobernig  <stefan.sobernig@wu.ac.at>


	Fix YAML syntax


	appveyor.yml: Provide for caching the tclkit running the build script,
	to improve robustness and availability

2019-03-20  Gustaf Neumann  <neumann@wu-wien.ac.at>


	handle ticket #3 on sourceforge: explicit "next" call in ensemble leads
	to unwanted "unknown" handler call

2019-03-18  Gustaf Neumann  <neumann@wu-wien.ac.at>


	since we know length, we can replace strcmp by memcmp

2019-03-17  Gustaf Neumann  <neumann@wu-wien.ac.at>


	trigger new travis build


	error message on stack overflow differs on windows and unix for tcl 8.5


	we see now a different error message in tcl8.5 for a recursive loop
	(drop test?)


	improve type cleanness


	- deactivate under windows for the time being


	make test more robust ("file lstat" returns less data under windows)


	added an additional variant of ALLOC_ON_STACK
	The new version does not use alloca(), it does not use VLA
	and it does not boat the stack in case huge vectors are to be processed.
	It uses a plain array up to a certain size and switches to malloc()
	above this size.

2019-03-16  Gustaf Neumann  <neumann@wu-wien.ac.at>


	improve spelling


	improve spelling


	unify spelling of "subclass"


	improve spelling


	align names (use "subclass" instead of "sub-class")


	align behavior of "current activelevel" with "... callinglevel" in case
	no NSF frame is found


	fix typos and make spelling more uniform


	add regression test for testing the behavior of :upvar from toplevel
	tclsh with and without filters
	This test covers implicitly also the behavior of [current callinglevel].


	conservative fix for "current callinglevel"; probably more to come

2019-03-15  Stefan Sobernig  <stefan.sobernig@wu.ac.at>


	  * nx.tcl, xotcl2.tcl: Fully qualify uses of Tcl's upvar and uplevel, to
	avoid confusion when introducing equally named procs/ cmds in the OS
	namespaces.

2019-03-14  Gustaf Neumann  <neumann@wu-wien.ac.at>


	minor cleanup: factor out common strings

2019-03-13  Gustaf Neumann  <neumann@wu-wien.ac.at>


	improve spelling and formatting


	improve spelling


	improve spelling


	- improved handling of object property autonamed
	A call of "new" calls internally the "create" method. When the
	"create" method is overloaded, we want to be able to check already
	on this level, whether the object is autonamed or not. The previous
	version has set the property at the end of the "new" method, which
	was too late for ttrace.


	xotcl1 compatibility: preserve overwritte slot accessor methods via
	instprocs
	many thanks to Zoran for indicating this

2019-03-12  Stefan Sobernig  <stefan.sobernig@wu.ac.at>


	  * win/makefile.vc: COMDAT folding can lead to unusable, pointless
	function-pointer comparisons (Nsf_ConvertToSwitch vs.
	Nsf_ConvertToBoolean). Reported for /opt:icf under Visual Studio 2017
	under x86. Kudos to Ashok P. Nadkarni.

2019-03-11  Gustaf Neumann  <neumann@wu-wien.ac.at>


	object property volatile: provide support for tuning volatile on/off
	via object property.

2019-03-10  Gustaf Neumann  <neumann@wu-wien.ac.at>


	improve spelling


	improve spelling


	improve spelling


	improve spelling


	windows: make sure, the install directory exists before copying data to
	it
	many thanks to Marc Gutman, who provided the patch


	momgoDB interface: add option "-asJSON" to "find all" to ease
	interaction e.g. with single page applications.


	improve spelling


	xotcl regression test: Added a more complex test for testing "new" +
	volatile

2019-03-09  Gustaf Neumann  <neumann@wu-wien.ac.at>


	add minimal regression test for object property "autonamed"


	make code assumptions clear by adding asserts.
	This was triggered by a report of Ashok running into a
	crash during startup on windows (Visual Studio 2017)

2019-03-08  Gustaf Neumann  <neumann@wu-wien.ac.at>


	Adjust regression test to more specific error message


	- improved error message "not allowed to have default":
	  make clear, this is from a parameter specification
	- added object property "autonamed" (set automatically for
	  objects created via "new")
	- xotcl2 volatile: improved backward compatibility with
	  XOTcl 1
	- extended regression test

2019-03-07  Gustaf Neumann  <neumann@wu-wien.ac.at>


	- added new variant of "volatile" trying to mimic the
	  XOTcl1 volatile behavior.
	- added experimental feature NSF_WITH_TCL_OBJ_TYPES_AS_CONVERTER, which
	  uses registered TclObjTypes as value checkers (currently deactivated)


	whitespace change

2019-02-18  Gustaf Neumann  <neumann@wu-wien.ac.at>


	protect legacy HTTPd against XSS on error messages

2019-02-06  Gustaf Neumann  <neumann@wu-wien.ac.at>


	base package nx::zip on Tcl 8.6 builtins instead of relying on the Trf
	package

2019-01-15  Gustaf Neumann  <neumann@wu-wien.ac.at>


	use "nonnull_assert" only in combination with "nonnull" declaration

2019-01-14  Stefan Sobernig  <stefan.sobernig@wu.ac.at>


	  * .travis.yml, appveyor.yml: Update build array to 8.6.9


	  * nsf-cmd.test: Make new tests 8.5-savy


	  * nsf.c (NsfProcStub): Re-order logic, so that the availability of a
	shadow proc cmd is tested first (re-fetch) and the parameter passing
	comes second, conditional on an available dispatch target.


	  * nsf-cmd.test: Add test rename (target) conflicts for the shadowed
	  procs; improve test formatting


	nsf.c (NsfStubProc): Improve comment formatting

2019-01-11  Stefan Sobernig  <stefan.sobernig@wu.ac.at>


	  * nsf-cmd.test: Modernize tests to work on error codes.


	  * nsf.c, nsf-cmd.test: Fixed nsf::procs for (unintended) deletes of the
	  shadowed proc, plus test cases.


	  * nsf.c, nsfShadow.c (NsfProcStubDeleteProc, Nsf_RenameObjCmd): Provide
	  for coupled renaming of the nsf::proc pairs; and coupled deletion.


	  * nsf.c (NsfProcStub, InvokeShadowedProc): Provide for
	    re-fetching (e.g., deleted) ::nsf::procs::* commands, to
	  allow for renamed nsf::procs to run.
	  * nsf-cmd.test: Added test to cover re-fetch


	  * nsf.c, nsfInt.h (NsfProcStub, NsfProcClientData): Apply
	  namespace reform to nsf::procs, to prevent proc
	  redefinition after a proc rename to fail.
	  * nsf-cmd.test: Add test case to document/ to cover failing rename +
	  redefine.

2018-12-22  Gustaf Neumann  <neumann@wu-wien.ac.at>


	improve code documentation


	remove unneeded assignment

2018-12-15  Gustaf Neumann  <neumann@wu-wien.ac.at>


	reduce dead assignments and variable scopes


	make sure, the restted path ends with a slash

2018-12-14  Gustaf Neumann  <neumann@wu-wien.ac.at>


	security fix: avoid directory traversal attack in old XOTcl HTTP class

2018-11-18  Gustaf Neumann  <neumann@wu-wien.ac.at>


	regenerated configure script


	improve handling of HAVE_INTTYPES_H under windows


	make sure, macros HAVE_INTPTR_T and HAVE_UINTPTR_T are set (should
	probably upgrade to newer TEA version)


	fix typo

2018-11-17  Gustaf Neumann  <neumann@wu-wien.ac.at>


	guard definition of PRIxPTR by HAVE_INTTYPES_H and not by HAVE_STDINT_H


	improve type cleanness


	fix typo

2018-11-04  Gustaf Neumann  <neumann@wu-wien.ac.at>


	remove comma before "that"


	use consistently US spelling variation

2018-11-03  Gustaf Neumann  <neumann@wu-wien.ac.at>


	remove comma before that

2018-10-24  Stefan Sobernig  <stefan.sobernig@wu.ac.at>


	  * disposition.test, parameters.test: Adjust tests to reflect the
	changed representational behavior for numerics (int, wide) according
	to TIP 514 (now in Tcl 8.7a2+).

2018-10-23  Gustaf Neumann  <neumann@wu-wien.ac.at>


	improve spelling


	help static analyzer to parse statements

2018-10-12  Gustaf Neumann  <neumann@wu-wien.ac.at>


	use consistently US spelling variants

2018-10-11  Stefan Sobernig  <stefan.sobernig@wu.ac.at>


	  * Makefile.in, win/makefile.vc: Add TCL_PKG_PREFER_LATEST to avoid
	  version hickups (stable beating latest) somewhere from TCLLIBPATH etc.

2018-10-02  Gustaf Neumann  <neumann@wu-wien.ac.at>


	update README.release to make sure, all version numbers in configure.ac
	are updated properly

2018-09-30  Stefan Sobernig  <stefan.sobernig@wu.ac.at>


	  * configure: Make sure configure is stashed with repo, as autotools are
	  not automatically re-generated (e.g., in build array)


	  * object-system.test: Add a simple test to catch incomplete version
	  bumps earlier

2018-09-29  Gustaf Neumann  <neumann@wu-wien.ac.at>


	bump also NSF_MINOR_VERSION after release

2018-09-28  Gustaf Neumann  <neumann@wu-wien.ac.at>


	follow the Tcl, not the OpenACS numbering scheme


	prefer US american spelling variants


	change version number to first version number after the 2.2.0 release
	(2.3d0)


	fix usernames at sourceforge
