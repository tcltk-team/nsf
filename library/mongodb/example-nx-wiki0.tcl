package require nx::mongo
package require nx::serializer
package require nx::test

# Establish connection to the database
::nx::mongo::db connect -db "tutorial"

# Make sure, we start always from scratch, so remove everything from
# the collection.
nx::mongo::db remove tutorial.pages {}

nx::mongo::Class create nx::mongo::Content {
  :property text:optional
  :property mime_type:optional
}

# item_id, (revision_id != 0) ?
nx::mongo::Class create nx::mongo::Page {
  :index tags
  :property name:required
  :property title:optional
  :property creator:optional
  :property page_order:optional
  :property description:optional
  :property nls_language:optional            ;# en, es, de, ... | locale: en_US, ... 
  :property creation_user:integer,required   ;# openacs-id? openacs-fk? user_id?
  :property publish_date:required
  :property publish_status:required
  :property content:embedded,type=::nx::mongo::Content

  :property page_template:optional          ;# openacs-id?

  :property instance_attributes:optional    ;# attributes? 

  :property assignee                        ;# openacs-id? openacs-fk? user_id?
  :property state

  :property form
  :property form_constraints

  :property tags:incremental,0..n
}

