package require nx::mongo
::nx::mongo::db connect -db "testing"

#
# Simplified example
#
nx::mongo::db drop collection posts
nx::mongo::Class create Post {
    :index id
    :index replyToId
    :property -accessor public id:required
    :property -accessor public replyToId:required
    :property -accessor public subject
    :property -accessor public body
    :property -accessor public author:required
    :property -accessor public {rating 0}
}

#
# Create a few posting objects in memory and save these
#
Post new -id 1 -replyToId 0 -rating 1 -subject "p1" -author rl
Post new -id 2 -replyToId 1 -rating 2 -subject "re: p1" -author gn
Post new -id 3 -replyToId 1 -rating 0 -subject "re: p1" -author aa
Post new -id 4 -replyToId 1 -rating 1 -subject "re: p1 (in Österreich)" -author bb
foreach p [Post info instances] {$p save}

#
# Query replies to "1" and try different sortings
#
set id 1
set orderby rating
Post show -cond "replyToId = $id" -orderby "{$orderby desc}"
set orderby id
Post show -cond "replyToId = $id" -orderby "{$orderby desc}"

#
# Print posting objects in memory as JSON
#
foreach p [Post info instances] { 
    puts [mongo::json::generate [$p bson encode]]
}

